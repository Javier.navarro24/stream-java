package stream;

import java.util.ArrayList;
import java.util.List;

public class Stream {
//uso de Stream con lambda
	private ArrayList<String> perifericos ;

	public Stream() {
		perifericos = new ArrayList();
		perifericos.add("raton");
		perifericos.add("teclado");
		perifericos.add("cascos");
		perifericos.add("microfono");
		perifericos.add("alfombrilla");
	}



	public void ordenar() { //ordenamos por oden alfabetico
		perifericos.stream().sorted().forEach(p -> System.out.println(p));

	}

	public void filtrar() {//filtramos por una letra al inicio
		perifericos.stream().filter(p -> p.startsWith("r")).forEach(p -> System.out.println(p));
	}

	public void limitar(){ //limitamos a 3 la lista
		perifericos.stream().limit(3).forEach(p -> System.out.println(p));
	}

	public void contar() { //contamos los componentes
		System.out.println(perifericos.stream().count()); 
	}
	
	public void transformar() { //transformamos a mayusculas
		perifericos.stream().map(String::toUpperCase).forEach(p -> System.out.println(p));
	}
	public static void main(String[] args) {

		Stream s = new Stream();
		System.out.println("Lista ordenada -->");
		s.ordenar();
		System.out.println("Lista filtrada por primera letra -->");
		s.filtrar();
		System.out.println("Lista limitada a 3  -->");
		s.limitar();
		System.out.println("Contamos los componentes de la lista -->");
		s.contar();
		System.out.println("Lista trasnformada a mayusculas -->");
		s.transformar();

	}
}
